use fatfs::Write;
use hal::gpio::OutputPin;

/// This module implements the traits that are required for Fatfs to
/// interface with the card.
use super::*;

impl<'t, CS: OutputPin> fatfs::IoBase for Card<'t, CS> {
    type Error = fatfs::Error<CardError>;
}

impl<'t, CS: OutputPin> fatfs::Write for Card<'t, CS> {
    /// Write a buffer into this writer, returning how many bytes were written.
    /// Each call to write may generate an I/O error indicating that the operation could not be completed. If an error
    /// is returned then no bytes in the buffer were written to this writer.
    /// It is not considered an error if the entire buffer could not be written to this writer.
    fn write(&mut self, buf: &[u8]) -> Result<usize, Self::Error> {
        // Convenience local copy
        let block_size = *self.block_size.as_ref().unwrap() as u64;

        // Addresses of the boundaries of the requested data
        let req_data_start = self.cursor;
        let req_data_end = req_data_start + buf.len() as u64 - 1;

        // Addresses of the blocks that are needed to satisfy request
        let needed_blocks_start = req_data_start / block_size * block_size;
        let needed_blocks_end = req_data_end / block_size * block_size + block_size - 1;

        // Size of slice of blocks to read (needed blocks capped at size of buffer)
        let buffer_blocks_start = needed_blocks_start;
        let buffer_blocks_end = u64::min(
            needed_blocks_end,
            needed_blocks_start + self.block_buf.len() as u64 - 1,
        );

        // Read blocks from the card
        self.read_block(needed_blocks_start)?;

        // Locate position of provided data into block_buf
        let data_start = (req_data_start - buffer_blocks_start) as usize;
        let data_end = (u64::min(req_data_end, buffer_blocks_end) - buffer_blocks_start) as usize;

        // Copy provided data into block_buf
        let mut j = 0;
        for i in data_start..data_end + 1 {
            self.block_buf[i] = buf[j];
            j += 1;
        }

        // Mark content of buffer as modified
        self.buf_dirty = true;

        // Moving cursor
        self.cursor += j as u64;

        // return number of bytes provided, guaranteed to be < buf.len()
        Ok(j)
    }

    /// Flush this output stream, ensuring that all intermediately buffered contents reach their destination.
    /// It is considered an error if not all bytes could be written due to I/O errors or EOF being reached.
    fn flush(&mut self) -> Result<(), Self::Error> {
        if let Some(addr) = self.buf_addr {
            if self.buf_dirty {
                self.write_block(addr)?;
                self.buf_dirty = false;
            }
        }
        Ok(())
    }
}

impl<'t, CS: OutputPin> fatfs::Read for Card<'t, CS> {
    /// Pull some bytes from this source into the specified buffer, returning how many bytes were read.
    ///
    /// This function does not provide any guarantees about whether it blocks waiting for data, but if an object needs
    /// to block for a read and cannot, it will typically signal this via an Err return value.
    ///
    /// If the return value of this method is `Ok(n)`, then it must be guaranteed that `0 <= n <= buf.len()`. A nonzero
    /// `n` value indicates that the buffer buf has been filled in with n bytes of data from this source. If `n` is
    /// `0`, then it can indicate one of two scenarios:
    ///
    /// 1. This reader has reached its "end of file" and will likely no longer be able to produce bytes. Note that this
    ///    does not mean that the reader will always no longer be able to produce bytes.
    /// 2. The buffer specified was 0 bytes in length.
    ///
    /// It is not an error if the returned value `n` is smaller than the buffer size, even when the reader is not at
    /// the end of the stream yet. This may happen for example because fewer bytes are actually available right now
    /// (e. g. being close to end-of-file) or because read() was interrupted by a signal.
    ///
    /// If this function encounters any form of I/O or other error, an error will be returned. If an error is returned
    /// then it must be guaranteed that no bytes were read.
    /// An error for which `IoError::is_interrupted` returns true is non-fatal and the read operation should be retried
    /// if there is nothing else to do.
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, <Card<'t, CS> as fatfs::IoBase>::Error> {
        // Convenience local copy
        let block_size = *self.block_size.as_ref().unwrap() as u64;

        // Addresses of the boundaries of the requested data
        let req_data_start = self.cursor;
        let req_data_end = req_data_start + buf.len() as u64 - 1;

        // Addresses of the blocks that are needed to satisfy request
        let needed_blocks_start = req_data_start / block_size * block_size;
        let needed_blocks_end = req_data_end / block_size * block_size + block_size - 1;

        // Size of slice of blocks to read (needed blocks capped at size of buffer)
        let buffer_blocks_start = needed_blocks_start;
        let buffer_blocks_end = u64::min(
            needed_blocks_end,
            needed_blocks_start + self.block_buf.len() as u64 - 1,
        );

        // Read blocks from the card
        self.read_block(needed_blocks_start)?;

        // Locate requested data into block_buf
        let data_start = (req_data_start - buffer_blocks_start) as usize;
        let data_end = (u64::min(req_data_end, buffer_blocks_end) - buffer_blocks_start) as usize;

        // Copy returned data into buf
        let mut j = 0;
        for i in data_start..data_end + 1 {
            buf[j] = self.block_buf[i];
            j += 1;
        }

        // Moving cursor
        self.cursor += j as u64;

        // return number of bytes provided, guaranteed to be < buf.len()
        Ok(j)
    }
}

/// Seek to an offset, in bytes, in a stream.
/// A seek beyond the end of a stream or to a negative position is not allowed.
/// If the seek operation completed successfully, this method returns the new position from the start of the
/// stream. That position can be used later with `SeekFrom::Start`.
/// Seeking to a negative offset is considered an error.
impl<'t, CS: OutputPin> fatfs::Seek for Card<'t, CS> {
    fn seek(&mut self, pos: fatfs::SeekFrom) -> Result<u64, Self::Error> {
        let capacity = self.capacity.unwrap();
        match pos {
            fatfs::SeekFrom::Start(offset) => {
                // Check that we don't go past the end
                if offset < capacity {
                    self.cursor = offset;
                } else {
                    return Err(fatfs::Error::Io(CardError::OutOfBounds));
                }
                Ok(self.cursor)
            }
            fatfs::SeekFrom::End(offset) => {
                // Check that we don't go below 0
                if offset <= 0 && (offset.unsigned_abs() as u64) < capacity {
                    self.cursor = capacity - (offset.unsigned_abs() as u64) - 1;
                } else {
                    return Err(fatfs::Error::Io(CardError::OutOfBounds));
                }
                Ok(self.cursor)
            }
            fatfs::SeekFrom::Current(offset) => {
                // Check that we don't go below 0
                if offset < 0 && (offset.unsigned_abs() as u64) <= self.cursor {
                    self.cursor -= offset.unsigned_abs() as u64;
                // Check that we don't go past the end
                } else if offset >= 0 && (offset as u64) <= (capacity - self.cursor - 1) {
                    self.cursor += offset as u64;
                } else {
                    return Err(fatfs::Error::Io(CardError::OutOfBounds));
                }
                Ok(self.cursor)
            }
        }
    }
}

/// Flush the buffer when disconnecting the card
/// to make sure that all modification are written
impl<'t, CS: OutputPin> Drop for Card<'t, CS> {
    fn drop(&mut self) {
        match self.flush() {
            Ok(_) => {}
            Err(_) => {
                panic!();
            }
        };
    }
}
