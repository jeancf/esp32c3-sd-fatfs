#![allow(dead_code)]
// Names of commands and their opcode
pub(crate) const GO_IDLE_STATE: u8 = 0;
pub(crate) const SEND_OP_COND: u8 = 1;
pub(crate) const SWITCH_FUNC: u8 = 6;
pub(crate) const SEND_IF_COND: u8 = 8;
pub(crate) const SEND_CSD: u8 = 9;
pub(crate) const SEND_CID: u8 = 10;
pub(crate) const STOP_TRANSMISSION: u8 = 12;
pub(crate) const SEND_STATUS: u8 = 13;
pub(crate) const SET_BLOCKLEN: u8 = 16;
pub(crate) const READ_SINGLE_BLOCK: u8 = 17;
pub(crate) const READ_MULTIPLE_BLOCK: u8 = 18;
pub(crate) const WRITE_BLOCK: u8 = 24;
pub(crate) const WRITE_MULTIPLE_BLOCK: u8 = 25;
pub(crate) const PROGRAM_CSD: u8 = 27;
pub(crate) const SET_WRITE_PROT: u8 = 28;
pub(crate) const CLR_WRITE_PROT: u8 = 29;
pub(crate) const SEND_WRITE_PROT: u8 = 30;
pub(crate) const ERASE_WR_BLK_START_ADDR: u8 = 32;
pub(crate) const ERASE_WR_BLK_END_ADDR: u8 = 33;
pub(crate) const ERASE: u8 = 38;
pub(crate) const LOCK_UNLOCK: u8 = 42;
pub(crate) const APP_CMD: u8 = 55;
pub(crate) const GEN_CMD: u8 = 56;
pub(crate) const READ_OCR: u8 = 58;
pub(crate) const CRC_ON_OFF: u8 = 59;
// Application-specific commands
pub(crate) const SD_STATUS: u8 = 0x80 + 13;
pub(crate) const SEND_NUM_WR_BLOCKS: u8 = 0x80 + 22;
pub(crate) const SET_WR_BLK_ERASE_COUNT: u8 = 0x80 + 23;
pub(crate) const SD_SEND_OP_COND: u8 = 0x80 + 41;
pub(crate) const SET_CLR_CARD_DETECT: u8 = 0x80 + 42;
pub(crate) const SEND_SCR: u8 = 0x80 + 51;

// For building command byte
pub(crate) const TRANSMISSION_BIT: u8 = 0x40;

/// State returned by the successful execution of Card::send_command()
#[derive(Debug)]
pub enum CardState {
    Idle,
    Ready,
}

/// Type of card detected
/// Ver2Sd - Standard capacity SD Card (SD) up to 2 GB
/// Ver2Hc - High Capacity SD Card (SDHC, SDXC)
/// Ver1xSd - Older SD Card
#[derive(Clone)]
pub(crate) enum CardType {
    Ver1xSd,
    Ver20Sd,
    Ver20Hc,
}
