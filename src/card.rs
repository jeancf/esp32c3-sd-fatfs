#![allow(dead_code)]

use core::convert::TryInto;

use super::*;

use hal::{
    clock::Clocks,
    gpio::*,
    pac::SPI2,
    prelude::*,
    spi::{self, Spi, SpiMode},
    system::PeripheralClockControl,
    systimer::SystemTimer,
};

/// System timer runs with a fixed clock frequency of 16 MHz
const SYS_TIM_FREQ: u32 = 16_000_000;

/// The Card struct implements a low level interface with SD Card or MultiMediaCard.
/// It initializes the communication with the card and provides an implementation of the
/// Read, Write and Seek IO traits that FAT FS uses to manipulate files and the filesystem.
pub struct Card<'t, CS: OutputPin> {
    clocks: &'t Clocks,
    spi: Spi<SPI2>,
    cs: CS,
    // if `ty` is Some(), the card has been initialized correctly
    ty: Option<CardType>,
    // Card-Specific Data register (CSD)
    csd: Option<[u8; 16]>,
    // Size of block for read and write operations in bytes
    pub(crate) block_size: Option<u16>,
    // Card capacity calculated fom CSD data in bytes
    pub(crate) capacity: Option<u64>,
    // Position of the read/write cursor in the user area of the card
    pub(crate) cursor: u64,
    // Buffer to hold data read from card
    pub(crate) block_buf: [u8; 512],
    // Address of first byte in buffer
    pub(crate) buf_addr: Option<u64>,
    // Track if buffer content needs to be written to card
    pub(crate) buf_dirty: bool,
    // List of partitions
    pub partitions: Option<[Partition; 4]>,
}

impl<'t, CS: OutputPin> Card<'t, CS> {
    /// Configures the SPI interface on the micro-controller
    /// used to communicate with the SD Card. Allocates a 512 bytes buffer
    /// for read and write operations
    pub fn new<SCK, MISO, MOSI>(
        clocks: &'t Clocks,
        p_spi: SPI2,
        mut cs: CS,
        mut sck: SCK,
        mut miso: MISO,
        mut mosi: MOSI,
        pcc: &mut PeripheralClockControl,
    ) -> Self
    where
        SCK: OutputPin,
        MISO: InputPin,
        MOSI: OutputPin,
    {
        // Configure pins
        cs.set_to_push_pull_output();
        sck.set_to_push_pull_output();
        miso.set_to_input();
        mosi.set_to_push_pull_output();

        // Configure SPI mode
        // "SPI mode 0 (CPHA=0, CPOL=0) is the proper setting to control MMC/SDC"
        // http://elm-chan.org/docs/mmc/mmc_e.html#spimode
        let mode = SpiMode::Mode0;

        // configure SPI host interface. We will control CS ourselves
        let spi = spi::Spi::new_no_cs(p_spi, sck, mosi, miso, 400_u32.kHz(), mode, pcc, clocks);
        // Handshake with the card must happen with a clock speed set to 100-400 KHz.
        // Once the handshake is completed, the frequency can be increased up to 20-25 MHz.

        Self {
            clocks,
            spi,
            cs,
            ty: None,
            csd: None,
            block_size: None,
            capacity: None,
            cursor: 0,
            block_buf: [0_u8; 512],
            buf_addr: None,
            buf_dirty: false,
            partitions: None,
        }
    }

    /// Set Carrier Select pin to high
    fn cs_high(&mut self) {
        self.cs.set_output_high(true);
    }

    /// Set Carrier Select pin to low
    fn cs_low(&mut self) {
        self.cs.set_output_high(false);
    }

    /// Exchange a byte
    fn exchange(&mut self, byte: u8) -> Result<u8, CardError> {
        let mut subval = [byte];
        let retval = self.spi.transfer(&mut subval);
        match retval {
            Ok(ba) => Ok(ba[0]),
            // Error is Infallible so this will never happen
            Err(_) => Err(CardError::SpiUndefined),
        }
    }

    /// Blocks while waiting for card to be ready
    /// milliseconds: How long to wait
    fn wait_ready(&mut self, milliseconds: u64) -> Result<(), CardError> {
        // Config timer to wait `milliseconds` milliseconds
        let deadline_tick =
            SystemTimer::now() + milliseconds * SystemTimer::TICKS_PER_SECOND / 1000;

        // Send 0xFF and check returned value until it is 0xFF
        while SystemTimer::now() <= deadline_tick {
            if self.exchange(0xff)? == 0xff {
                return Ok(());
            };
        }
        Err(CardError::WaitReadyTimeOut)
    }

    /// Allow card to stabilize then send clock cycles
    fn power_up(&mut self) -> Result<(), CardError> {
        // Wait 1 ms
        let deadline_tick = SystemTimer::now() + SystemTimer::TICKS_PER_SECOND / 1000;
        while SystemTimer::now() <= deadline_tick {}

        self.cs_high();

        // Transmit 80 dummy bytes
        self.transmit_dummy_bytes(80)?;

        Ok(())
    }

    fn deselect(&mut self) -> Result<(), CardError> {
        self.cs_high();
        self.exchange(0xff)?;
        Ok(())
    }

    /// Put the card in a state ready to receive SPI commands
    fn select(&mut self) -> Result<(), CardError> {
        self.cs_low();

        if let Err(e) = self.wait_ready(500) {
            self.deselect()?;
            return Err(e);
        }
        Ok(())
    }

    /// Receive a block of data from the Card and store it in buffer
    /// byte_cnt: number of bytes to read
    fn receive_block(&mut self, byte_cnt: usize) -> Result<(), CardError> {
        if byte_cnt > self.block_buf.len() {
            return Err(CardError::BufferOverflow);
        }

        // Launch timer to measure 200 ms
        let deadline_tick = SystemTimer::now() + 200 * SystemTimer::TICKS_PER_SECOND / 1000;

        loop {
            // Receive byte and check if it is the data start flag
            if self.exchange(0xff)? == 0xfe {
                break;
            }
            // check if the timer has ran out
            if SystemTimer::now() > deadline_tick {
                return Err(CardError::ReceiveBlockTimeOut);
            }
        }

        // Data start flag has been received, store received data in buffer
        for i in 0..byte_cnt {
            self.block_buf[i] = self.exchange(0xff)?;
        }

        // Receive and discard CRC
        self.exchange(0xff)?;
        self.exchange(0xff)?;

        Ok(())
    }

    /// Transmit a block of data to the Card
    /// buf: slice of buffer array to be transmitted
    fn transmit_block(&mut self, byte_cnt: usize, token: u8) -> Result<(), CardError> {
        // Cannot send more than what we have
        if byte_cnt > self.block_buf.len() {
            return Err(CardError::BufferOverflow);
        }

        self.wait_ready(500)?;

        // Send token and check that response is not the Stop Transmission flag
        if self.exchange(token)? == 0xfd {
            return Err(CardError::TransmitBlockStop);
        };

        // Transmit data
        for i in 0..byte_cnt {
            self.exchange(self.block_buf[i])?;
        }

        // Transmit dummy CRC
        self.exchange(0xff)?;
        self.exchange(0xff)?;

        // Verify that data was accepted
        if self.exchange(0xff)? & 0x1f != 0x05 {
            return Err(CardError::TransmitBlockRejected);
        }
        Ok(())
    }

    /// Convenience method to transmit n dummy bytes to the card consecutively
    fn transmit_dummy_bytes(&mut self, n: usize) -> Result<(), CardError> {
        for _ in 0..n {
            self.exchange(0xff)?;
        }
        Ok(())
    }

    /// Send command to the card
    /// Available commands are listed as const in commands.rs
    fn send_command(&mut self, mut command: u8, argument: u32) -> Result<CardState, CardError> {
        // If command is an application command (ACMD), APP_CMD must be sent first
        if command & 0x80 == 0x80 {
            command &= 0x7F;
            self.send_command(APP_CMD, 0)?;
        }

        // Select the card except if command is STOP_TRANSMISSION
        if command != STOP_TRANSMISSION {
            // Drive CS pin from high to low
            // and wait for card to be ready (MISO high)
            self.deselect()?;
            self.select()?;
        }

        // Send the command
        self.exchange(TRANSMISSION_BIT | command)?;

        // Send the 4 argument bytes
        self.exchange(((argument >> 24) & 0xff) as u8)?;
        self.exchange(((argument >> 16) & 0xff) as u8)?;
        self.exchange(((argument >> 8) & 0xff) as u8)?;
        self.exchange((argument & 0xff) as u8)?;

        // Send CRC and end bit
        let last_byte = match (command, argument) {
            // Valid CRC + end bit for specific commands
            (GO_IDLE_STATE, _) => 0x95,
            (SEND_IF_COND, _) => 0x87,
            (APP_CMD, _) => 0x65,
            (SD_SEND_OP_COND, 0x40000000) => 0x77, // With HCS bit set
            (SD_SEND_OP_COND, 0) => 0xe5,          // With HCS bit cleared
            // Dummy CRC + end bit
            _ => 0x01,
        };
        self.exchange(last_byte)?;

        // Discard next received byte after STOP_TRANSMISSION
        // (Not sure why)
        if command == STOP_TRANSMISSION {
            self.exchange(0xff)?;
        }

        // Get R1 response (or first byte of R3)
        let mut r: u8 = 0x80;
        // Send dummy bytes and listen for response
        for _ in 0..10 {
            r = self.exchange(0xff)?;
            // Bit 7 of valid response is 0
            if r & 0x80 == 0 {
                break;
            }
        }

        let result: Result<CardState, CardError> = match r {
            0 => Ok(CardState::Ready),
            1 => Ok(CardState::Idle),
            2 => Err(CardError::SendCmdEraseReset),
            4 => Err(CardError::SendCmdIllegalCommand),
            8 => Err(CardError::SendCmdCrc),
            16 => Err(CardError::SendCmdEraseSequence),
            32 => Err(CardError::SendCmdAddress),
            64 => Err(CardError::SendCmdParameter),
            _ => Err(CardError::SendCmdInvalidResponse),
        };
        result
    }

    /// Establish connection with SD card
    /// Returns Ok() if sd card is in card is ready
    pub fn connect(&mut self) -> Result<(), CardError> {
        // Prepare to communicate with card
        self.power_up()?;

        // Put card in SPI mode
        let r = self.send_command(GO_IDLE_STATE, 0x00);
        if let CardState::Ready = r.unwrap() {
            return Err(CardError::NotIdle);
        };

        // Check SD Card version
        // Argument bytes to be sent with SEND_IF_COND (CMD8)
        // Byte 1 = 00 reserved
        // Byte 2 = 00 reserved
        // Byte 3 = 01 voltage range
        // Byte 4 = aa verification token that card will send back in response
        match self.send_command(SEND_IF_COND, 0x01aa) {
            // Command is not recognized, We may have a 1x card
            Err(CardError::SendCmdIllegalCommand) => self.init_ver_1x()?,
            Ok(CardState::Idle) => self.init_ver_20()?,
            _ => return Err(CardError::SendIfCondInvalidResponse),
        }

        // Set CS to high
        self.deselect()?;

        // Set clock to fast
        // Once the handshake is completed, the frequency can be increased up to 20-25 MHz.
        self.spi.change_bus_frequency(20_u32.MHz(), self.clocks);

        // Wait 500 ms for new frequency to stabilize
        let deadline_tick = SystemTimer::now() + 500 * SystemTimer::TICKS_PER_SECOND / 1000;
        loop {
            if SystemTimer::now() > deadline_tick {
                break;
            }
        }

        // Read CSD register from card
        self.read_csd()?;

        // Define capacity of card
        self.calculate_capacity();

        // Populate list of partitions
        self.read_partitions()?;

        Ok(())
    }

    /// Initialize SD Card version 2
    fn init_ver_20(&mut self) -> Result<(), CardError> {
        // Read next 4 bytes of R7 returned by SEND_IF_COND
        // that containt the voltage range and verification token that we sent
        let mut r7 = [0x00_u8; 4];
        for r in r7.iter_mut() {
            *r = self.exchange(0xff)?;
        }

        // Continue if card voltage is compatible and verification token is correct
        if r7[2] == 0x01 && r7[3] == 0xaa {
            // Launch timer to measure 1 s initialization time
            let deadline_tick = SystemTimer::now() + 1000 * SystemTimer::TICKS_PER_SECOND / 1000;

            // Initialize the Card and wait for CardState::Ready or time-out
            loop {
                // Try SD_SEND_OP_COND first and fall back to SEND_OP_COND if it does not work
                if let CardState::Ready = self.send_command(SD_SEND_OP_COND, 1 << 30)? {
                    break;
                }
                // check if the timer has ran out
                if SystemTimer::now() > deadline_tick {
                    return Err(CardError::SdSendOpCondTimeOut);
                }
            }
            // Check if card is a High Capacity SD Card
            if let CardState::Ready = self.send_command(READ_OCR, 0)? {
                // Read the next 4 bytes of R3 returned by READ_OCR
                // that contain the OCR register
                let mut ocr = [0x00_u8; 4];
                for o in ocr.iter_mut() {
                    *o = self.exchange(0xff)?;
                }

                // Check if CCS bit (bit 30 of response) is set
                // (= bit 6 of first byte received)
                if ocr[0] & (0b1 << 6) == 0x40 {
                    self.ty = Some(CardType::Ver20Hc);
                } else {
                    self.ty = Some(CardType::Ver20Sd);
                }
            }

            // Block length of the read and write commands are fixed to 512 bytes in SDHC and SDXC cards
            self.block_size = Some(512);
        }
        // Card voltage is not compatible
        else {
            return Err(CardError::IncompatibleVoltage);
        }

        Ok(())
    }

    /// Initialize SD Card version 1x
    fn init_ver_1x(&mut self) -> Result<(), CardError> {
        // Launch timer to measure 1 s initialization time
        let deadline_tick = SystemTimer::now() + 1000 * SystemTimer::TICKS_PER_SECOND / 1000;

        // Initialize the Card and wait for CardState::Ready or time-out
        loop {
            //
            if let CardState::Ready = self.send_command(SD_SEND_OP_COND, 0)? {
                break;
            }
            // check if the timer has ran out
            if SystemTimer::now() > deadline_tick {
                return Err(CardError::SdSendOpCondTimeOut);
            }
        }
        self.ty = Some(CardType::Ver1xSd);

        // Set block length to 512 bytes
        self.send_command(SET_BLOCKLEN, 512)?;
        self.block_size = Some(512);
        // Block length of the read and write commands are fixed to 512 bytes in SDHC and SDXC cards
        // But they must be set explicitely for the standard capacity cards. Copied from:
        // https://github.com/Kevin-Sangeelee/gd32v_test/blob/master/gd32v_test/src/fatfs/tf_card.c

        Ok(())
    }

    /// Request contents of CSD register from card and store it in `csd` field of Card struct
    fn read_csd(&mut self) -> Result<(), CardError> {
        self.select()?;

        self.send_command(SEND_CSD, 0)?;

        // Get data and store it in the Card struct field
        match self.receive_block(16) {
            Ok(_) => {
                // Store in struct
                self.csd = Some(self.block_buf[0..16].try_into().unwrap());
                self.deselect()?;

                // Invalidate buffer content
                self.buf_addr = None;

                Ok(())
            }
            Err(e) => {
                self.deselect()?;
                Err(e)
            }
        }
    }

    /// Calculate capacity of user area in bytes and store it in `capacity` field of Card struct
    /// Capacity is necessary for fatfs::SeekFrom(End(i64)) that needs to know the end of the user area.
    /// Capacity is calculated based on data contained in the CSD register of the card.
    /// Call `Card::read_csd()` first.
    fn calculate_capacity(&mut self) {
        // Cannot calculate capacity if no CSD is available
        if self.csd.is_none() {
            return;
        }

        // This unwrap will not panic
        let csd = self.csd.as_ref().unwrap();

        // READ CSD_STRUCTURE (bits 126-127) to define which method to apply
        let csd_structure = csd[0] >> 6;

        let capacity: u64 = match csd_structure {
            // CSD version 2
            1 => {
                // Capacity = (C_SIZE+1) * 512K
                // C_SIZE is at bits [69-48] (22 bits) of CSD register:
                // byte 9, byte 8 and 6 lower bits of byte 7 of CSD array
                let mut c_size = csd[9] as u64;
                let byte = (csd[8] as u64) << 8;
                c_size |= byte;
                let byte = ((csd[7] as u64) & 0x3f) << 16;
                c_size |= byte;
                (c_size + 1) * 512 * 1024
            }
            // CSD version 1
            0 => {
                // C_SIZE is at bits [73-62] (12 bits)
                // C_SIZE_MULT is at bits [49-47] (3 bits)
                // READ_BL_LEN is at bits [83-80] (4 bits)
                // Capacity = block_nr * block_len
                // block_nr = (C_SIZE+1) * mult
                // mult = 2^(CSIZE_MULT+2)
                // block_len = 2^READ_BL_LEN

                // Isolate C_SIZE
                let mut c_size = (csd[8] >> 6) as u64;
                let byte = (csd[7] as u64) << 2;
                c_size |= byte;
                let byte = ((csd[6] & 0x3) as u64) << 10;
                c_size |= byte;

                // Isolate C_SIZE_MULT
                let mut c_size_mult = ((csd[10] & 0x80) >> 7) as u32;
                let byte = ((csd[9] & 0x3) as u32) << 1;
                c_size_mult |= byte;

                //Isolate READ_BL_LEN
                let read_bl_len = (csd[5] & 0xf) as u32;

                let mult = 2_u64.pow(c_size_mult + 2);
                let block_nr = (c_size + 1) * mult;
                let block_len = 2_u64.pow(read_bl_len);
                block_nr * block_len
            }
            _ => 0,
        };
        self.capacity = Some(capacity);
    }

    /// Read data blocks from the card to fill the buffer if the requested block is not already in buffer
    /// The size of the block to be read is defined by the format of the Card (typically 512 bytes)
    pub(crate) fn read_block(&mut self, mut addr: u64) -> Result<(), CardError> {
        self.select()?;

        // Local copy for convenience
        let block_size = *self.block_size.as_ref().unwrap() as usize;

        // Verify that block_size fits in buffer
        if block_size > self.block_buf.len() {
            return Err(CardError::BufferOverflow);
        }

        // SDHC and SDXC Cards (CCS=1) use block unit address (512 bytes unit).
        if let Some(CardType::Ver20Hc) = self.ty {
            addr /= 512;
        }

        if let Some(a) = self.buf_addr {
            // Check if requested block is in buffer already
            if a == addr {
                // Block already in buffer, nothing to do
                return Ok(());
            } else {
                // Buffer contains changes
                if self.buf_dirty {
                    // Write buffer to
                    self.write_block(a)?;
                    self.buf_dirty = false;
                }
            }
        }

        // Read one block
        if let CardState::Ready = self.send_command(READ_SINGLE_BLOCK, addr as u32)? {
            self.receive_block(block_size)?;
        }

        // Record address of block in buffer
        self.buf_addr = Some(addr);

        self.deselect()?;

        Ok(())
    }

    /// Write the full content of the block buffer to disk
    pub(crate) fn write_block(&mut self, mut addr: u64) -> Result<(), CardError> {
        self.select()?;

        // SDHC and SDXC Cards (CCS=1) use block unit address (512 bytes unit).
        if let Some(CardType::Ver20Hc) = self.ty {
            addr /= 512;
        }

        // Write one block
        if let CardState::Ready = self.send_command(WRITE_BLOCK, addr as u32)? {
            // Send the full content of block buffer
            // 0xfe is the token indicating a single block write
            self.transmit_block(512, 0xfe)?;
        }

        self.deselect()?;

        Ok(())
    }

    /// Try to read the Master Boot Record of the card to identify partitions
    // http://blog.hakzone.info/posts-and-articles/bios/analysing-the-master-boot-record-mbr-with-a-hex-editor-hex-workshop/
    fn read_partitions(&mut self) -> Result<(), CardError> {
        // Read first block on the Card
        self.read_block(0)?;

        // Verify the MBR signature
        if !(self.block_buf[510] == 0x55 && self.block_buf[511] == 0xAA) {
            return Err(CardError::MbrNotFound);
        }

        let mut partitions = [Partition::default(); 4];

        // Step through the 4 x 16 bytes entries of the partition table
        for (i, s) in (446..510).step_by(16).enumerate() {
            // Isolate slice of partition table entry
            let pte = &self.block_buf[s..s + 16];

            // Identify partition type

            let ptype = match pte[4] {
                0x00 => PartType::Empty,
                0x01 => PartType::FAT12,
                0x04 => PartType::FAT16,
                0x0c => PartType::FAT32,
                _ => PartType::Unsupported,
            };

            // Address of partition (offset from 0 in bytes)
            let start_byte = le_bytes_to_u64(&pte[8..12]) * 512;

            // Size of partition (in bytes)
            let size = le_bytes_to_u64(&pte[12..16]) * 512;

            partitions[i] = Partition {
                ptype,
                start_byte,
                size,
            };
        }

        self.partitions = Some(partitions);

        Ok(())
    }
}

/// Convert a slice of 4 bytes in little endian into a u64
fn le_bytes_to_u64(bytes: &[u8]) -> u64 {
    let mut result: u64 = 0;
    for (i, b) in bytes.iter().enumerate() {
        result += *b as u64 * (256_u64).pow(i as u32);
    }
    result
}
