/// Enumeration of the supported partition types
#[derive(Clone, Copy)]
pub enum PartType {
    FAT12,
    FAT16,
    FAT32,
    Empty,
    Unsupported,
}

impl Default for PartType {
    fn default() -> Self {
        PartType::Empty
    }
}

/// Store partition information retrieved from the Master Boot Record
#[derive(Default, Clone, Copy)]
pub struct Partition {
    pub ptype: PartType,
    pub start_byte: u64,
    pub size: u64,
}
