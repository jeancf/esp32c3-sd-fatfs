#![no_std]

#[doc = include_str!("../README.md")]
use esp32c3_hal as hal;

pub mod card;
pub use card::*;

mod commands;
use commands::*;

mod partitions;
use partitions::*;

pub mod fsslice;
pub use fsslice::*;

pub mod errors;
pub mod traits;

use errors::CardError;
