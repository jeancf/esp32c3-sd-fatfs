#![no_std]
#![no_main]

use riscv_rt::entry;

use esp32c3_sd_fatfs::Card;
use esp32c3_sd_fatfs::FsSlice;
use fatfs::{Read, Write};

use esp32c3_hal::{clock::ClockControl, pac::Peripherals, prelude::*, timer::TimerGroup, Rtc, IO};
use esp_backtrace as _;
use esp_println::*;

#[entry]
fn run() -> ! {
    // Initialize MCU
    let peripherals = Peripherals::take().unwrap();
    let mut system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    // Disable the RTC and TIMG watchdog timers
    let mut rtc_cntl = Rtc::new(peripherals.RTC_CNTL);
    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let mut wdt0 = timer_group0.wdt;
    let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
    let mut wdt1 = timer_group1.wdt;

    rtc_cntl.rwdt.disable();
    rtc_cntl.swd.disable();
    wdt0.disable();
    wdt1.disable();

    // Take ownership of the SPI peripheral
    let spi = peripherals.SPI2;

    // initialize GPIO pins for SPI
    let cs = io.pins.gpio4;
    let sck = io.pins.gpio5;
    let miso = io.pins.gpio6;
    let mosi = io.pins.gpio7;

    // Setup SPI host interface with the card
    let mut card = Card::new(
        &clocks,
        spi,
        cs,
        sck,
        miso,
        mosi,
        &mut system.peripheral_clock_control,
    );

    println!("Card interface created");

    // Establish connection with the card (which must be inserted at this point)
    match card.connect() {
        Ok(()) => {
            println!("Card::connect() successful");
        }
        Err(e) => {
            println!("Card::connect() failed: {}", e);
            panic!();
        }
    };

    // Select partition and wrap it into a slice
    let partition = FsSlice::primary_partition(card, 0).unwrap();

    // Initialize file system struct with FS slice
    let fs = match fatfs::FileSystem::new(partition, fatfs::FsOptions::new()) {
        Ok(fs) => fs,
        Err(e) => {
            println!("Error opening file system {}", e);
            panic!();
        }
    };

    let root_dir = fs.root_dir();

    // write a file in root directory
    let mut root_file = root_dir.create_file("rootfile.txt").unwrap();
    root_file.truncate().unwrap();
    root_file.write_all(b"Hello root World!\n").unwrap();
    root_file.flush().unwrap();
    println!("Created rootfile.txt");

    // write a file in a new directory
    root_dir.create_dir("newdir").unwrap();
    let mut new_file = root_dir.create_file("newdir/newfile.txt").unwrap();
    new_file.truncate().unwrap();
    new_file.write_all(b"Hello New World!\n").unwrap();
    new_file.flush().unwrap();
    println!("Created newdir/newfile.txt");

    // Iterate files in root directory
    for e in root_dir.iter() {
        // Get directory entry
        let de = e.unwrap();
        println!(
            "Entry Name: {:?}",
            core::str::from_utf8(de.short_file_name_as_bytes()).unwrap()
        );
        if de.is_file() {
            // Read file content
            let mut f = de.to_file();
            let mut content = [0 as u8; 17];
            let cnt = f.read(&mut content).unwrap();
            println!("{} bytes read from file", cnt);
            println!(
                "Read: {:?}",
                core::str::from_utf8(content.as_ref()).unwrap()
            );
        } else {
            println!("directory entry is not a file");
        }
    }

    // Open file in directory
    let new_dir = root_dir.open_dir("newdir").unwrap();
    let mut nf = new_dir.open_file("newfile.txt").unwrap();
    // Alternatively
    // let mut nf = root_dir.open_file("newdir/newfile.txt").unwrap();
    println!("newdir/newfile.txt open");

    // Read file content
    let mut content = [0 as u8; 18];
    let cnt = nf.read(&mut content).unwrap();
    println!("{} bytes read from file", cnt);
    println!(
        "Read: {:?}",
        core::str::from_utf8(content.as_ref()).unwrap()
    );

    println!("Execution Finished correctly");
    panic!("Reached the end of the world");
}
